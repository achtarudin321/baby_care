<?php

use Illuminate\Database\Seeder;
use App\Models\Tenant\Menu\MenuTenantModel;

class TenantMenuBeranda extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuTenantModel::create([
            'code' => 'beranda',
            'name' => 'Beranda',
            'url' => '/beranda',
            'icon' => '',
            'sort' => '1',
            'description' => 'Halaman Beranda',
            'type' => 'general'
        ]);
    }
}
