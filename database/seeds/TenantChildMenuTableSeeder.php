<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant\Menu\ChildMenuTenantModel;

class TenantChildMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ChildMenuTenantModel::create([
            'menu_tenant_id' => 2,
            'code' => 'Cbg',
            'name' => 'Cabang',
            'url' => '/branches',
            'sort' => 1,
            'description' => 'Child page branches',
            'is_active' => true,
        ]);

        ChildMenuTenantModel::create([
            'menu_tenant_id' => 2,
            'code' => 'Empl',
            'name' => 'Tambah Pengguna',
            'url' => '/employee',
            'sort' => 2,
            'description' => 'Child page branches',
            'is_active' => true,
        ]);

        ChildMenuTenantModel::create([
            'menu_tenant_id' => 3,
            'code' => 'Acc',
            'name' => 'Akun',
            'url' => '/account',
            'sort' => 1,
            'description' => 'Child page setting',
            'is_active' => true,
        ]);
    }
}
