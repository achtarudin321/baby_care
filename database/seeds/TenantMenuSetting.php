<?php

use Illuminate\Database\Seeder;
use App\Models\Tenant\Menu\MenuTenantModel;

class TenantMenuSetting extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settingMenu = MenuTenantModel::create([
            'code' => 'setting',
            'name' => 'Setting',
            'url' => '',
            'icon' => '',
            'sort' => '3',
            'description' => 'Parent Setting',
            'type' => 'general'
        ]);

        $settingMenu->menu_children()->create([
            'code' => 'account',
            'name' => 'Akun',
            'url' => '/account',
            'sort' => 1,
            'description' => 'menu setting account',
            'is_active' => true,
        ]);
    }
}
