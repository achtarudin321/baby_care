<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Models\Tenant\Menu\MenuTenantModel;

class TenantMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        MenuTenantModel::create([
            'code' => 'brnd',
            'name' => 'Beranda',
            'url' => '/beranda',
            'icon' => '',
            'sort' => '1',
            'description' => 'Halaman Beranda',
            'type' => 'primary'
        ]);

        MenuTenantModel::create([
            'code' => 'adm',
            'name' => 'Admin',
            'url' => '',
            'icon' => '',
            'sort' => '2',
            'description' => 'Parent Admin',
            'type' => 'parent'
        ]);

        MenuTenantModel::create([
            'code' => 'stg',
            'name' => 'Setting',
            'url' => '',
            'icon' => '',
            'sort' => '3',
            'description' => 'Parent Setting',
            'type' => 'parent'
        ]);
    }
}
