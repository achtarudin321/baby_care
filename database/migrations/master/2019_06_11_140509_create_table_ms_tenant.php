<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Master\TenantMaster\TenantMasterModel;

class CreateTableMsTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('ms_tenant');
        Schema::create('ms_tenant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('npwp')->unique();
            $table->string('name');
            $table->string('owner');
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email');
            $table->text('address');
            $table->text('description');
            $table->string('db_host')->nullable();
            $table->string('db_port')->nullable();
            $table->string('db_name')->nullable();
            $table->string('db_username')->nullable();
            $table->string('db_password')->nullable();
            $table->boolean('is_active')->default(false);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('ms_users');
            $table->foreign('updated_by')->references('id')->on('ms_users');
        });

        $this->createTwoExmpleTenant();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_tenant');
    }

    protected function createTwoExmpleTenant () {
        TenantMasterModel::create([
            'code' => 'TN1',
            'npwp' => '876543210',
            'name' => 'Tenant 1',
            'owner' => 'Tenat Contoh 1',
            'phone' => '021111111111',
            'fax' => '02111111111',
            'email' => 'tenan1t@tenant1.com',
            'address' => 'Tenant Satu Untuk Contoh 1',
            'description' => 'Tenant Satu Hanya Untuk Contoh 1',
            'db_host' => '127.0.0.1',
            'db_port' => '3306',
            'db_name' => 'inv_tenant_1',
            'db_username' => 'root',
            'db_password' => '',
            'created_by' => 1,
        ]);

        TenantMasterModel::create([
            'code' => 'TN2',
            'npwp' => '012345678',
            'name' => 'Tenant 2',
            'owner' => 'Tenat Contoh _2',
            'phone' => '02122222222',
            'fax' => '02122222222',
            'email' => 'tenant2@tenant2.com',
            'address' => 'Tenant Satu Untuk Contoh 2',
            'description' => 'Tenant Satu Hanya Untuk Contoh 2',
            'db_host' => '127.0.0.1',
            'db_port' => '3306',
            'db_name' => 'inv_tenant_2',
            'db_username' => 'root',
            'db_password' => '',
            'created_by' => 1,
        ]);
    }
}
