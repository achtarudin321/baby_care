<?php

/**
 * Part of the Sentinel package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel
 * @version    2.0.17
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2017, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
// use Sentinel;
// use Sentinel;

class MigrationCartalystSentinel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::dropIfExists('ms_users');
        Schema::create('ms_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('username');
            $table->string('password');
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->text('permissions')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->string('api_token', 80)
            ->unique()
            ->nullable()
            ->default(null);
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->unique('email');
            $table->unique('username');
        });

        Schema::dropIfExists('ms_activations');
        Schema::create('ms_activations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('code');
            $table->boolean('completed')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->foreign('user_id')->references('id')->on('ms_users');

        });

        Schema::dropIfExists('ms_persistences');
        Schema::create('ms_persistences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('code');
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->unique('code');
            $table->foreign('user_id')->references('id')->on('ms_users');
        });

        Schema::dropIfExists('ms_reminders');
        Schema::create('ms_reminders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('code');
            $table->boolean('completed')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->foreign('user_id')->references('id')->on('ms_users');
        });

        Schema::dropIfExists('ms_roles');
        Schema::create('ms_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
            $table->text('permissions')->nullable();
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->unique('slug');
        });

        Schema::dropIfExists('ms_role_users');
        Schema::create('ms_role_users', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->nullableTimestamps();

            $table->engine = 'InnoDB';
            $table->primary(['user_id', 'role_id']);
            $table->foreign('user_id')->references('id')->on('ms_users');
            $table->foreign('role_id')->references('id')->on('ms_roles');
        });

        Schema::dropIfExists('ms_throttle');
        Schema::create('ms_throttle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('type');
            $table->string('ip')->nullable();
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->index('user_id');
            $table->foreign('user_id')->references('id')->on('ms_users');
        });

        $credentials = [
            'username' => 'master',
            'email'    => 'master@master.com',
            'password' => 'secret',
            'permissions' => [
                'user.master' => true
            ],
            'first_name' => 'user',
            'middle_name' => 'admin',
            'last_name' => 'master'
        ];

        Sentinel::registerAndActivate($credentials);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('ms_activations');
        Schema::dropIfExists('ms_persistences');
        Schema::dropIfExists('ms_reminders');
        Schema::dropIfExists('ms_roles');
        Schema::dropIfExists('ms_role_users');
        Schema::dropIfExists('ms_throttle');
        Schema::dropIfExists('ms_users');
        Schema::disableForeignKeyConstraints();
    }
}
