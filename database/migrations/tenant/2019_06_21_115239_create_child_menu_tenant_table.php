<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildMenuTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_menu_child', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_tenant_id')->unsigned();
            $table->string('code');
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->integer('sort')->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_active')->nullable()->default(true);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('menu_tenant_id')->references('id')->on('tenant_menu');
            $table->foreign('created_by')->references('user_id')->on('tn_user_tenant');
            $table->foreign('updated_by')->references('user_id')->on('tn_user_tenant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_menu_child');
    }
}
