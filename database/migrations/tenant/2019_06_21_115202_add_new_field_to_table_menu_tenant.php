<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldToTableMenuTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_menu', function (Blueprint $table) {
            $table->integer('sort')->nullable()->after('icon');
            $table->string('code')->nullable()->change();
            $table->string('name')->nullable()->change();
            $table->string('url')->nullable()->change();
            $table->text('description')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_menu', function (Blueprint $table) {

        });
    }
}
