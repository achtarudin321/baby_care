<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

Route::get('/', 'Home\HomeController@index');

//  Route untuk Master
Route::middleware(['isLogin', 'isNonTenant'])->group(function () {

    Route::get('dashboard', 'Dashboard\DashboardController@index');

    Route::get('tenant', 'Tenant\TenantController@index');
    Route::get('tenant/create', 'Tenant\TenantController@create');
    Route::post('tenant/store', 'Tenant\TenantController@store');
    Route::get('tenant/{id}', 'Tenant\TenantController@show');
    Route::get('tenant-enabled/{id}', 'Tenant\TenantController@enableTenant');
    Route::get('tenant-disabled/{id}', 'Tenant\TenantController@disableTenant');
    Route::get('tenant/{id}/edit', 'Tenant\TenantController@edit');
    Route::put('tenant/{id}/update', 'Tenant\TenantController@update');


    Route::get('admin', 'Admin\AdminController@index');
    Route::get('admin/create', 'Admin\AdminController@create');
    Route::post('admin/store', 'Admin\AdminController@store');
    Route::get('admin/{email}', 'Admin\AdminController@show');
    Route::get('admin-enabled/{email}', 'Admin\AdminController@enableAdmin');
    Route::get('admin-disabled/{email}', 'Admin\AdminController@disableAdmin');
    Route::get('admin/edit/{email}', 'Admin\AdminController@edit');


});

// Route untuk Client ataupun Tenant
Route::middleware(['isLogin', 'isTenant'])->group(function () {

    Route::get('beranda', 'Beranda\BerandaController@index');

    Route::resource('branches', 'Branches\BranchesController');
    Route::get('branches-enabled/{email}', 'Branches\BranchesController@enableBranch');
    Route::get('branches-disabled/{email}', 'Branches\BranchesController@disableBranch');

    Route::resource('employee', 'Employee\EmployeeController');

    Route::resource('roles', 'Role\RoleController');


});

// Route untuk logout
Route::middleware(['isLogin'])->group(function () {

    Route::any('logout', 'Auth\logoutController@logoutUser');
    Route::get('account', 'Account\AccountController@index');
    Route::post('account/change-password', 'Account\AccountController@changePassword');

});

//  Route untuk login and Register
Route::middleware(['isNotLogin'])->group(function () {

    // Route::get('/register', 'Auth\RegisterController@index');
    // Route::post('/register', 'Auth\RegisterController@postRegister');

    Route::get('login', 'Auth\LoginController@index');
    Route::post('login', 'Auth\LoginController@postLogin');

    Route::post('forgot-password', 'Auth\ForgotPasswordController@forgotPassword');

    Route::get('reset-password/{email}/{reminderCode}', 'Auth\ResetPasswordController@resetPassword');
    Route::post('reset-password', 'Auth\ResetPasswordController@postResetPassword');

    Route::get('change-password', 'Auth\ChangePasswordController@changePassword');

    ROute::get('testing', 'Testing\TestingController@assignRoleToUser');
});



