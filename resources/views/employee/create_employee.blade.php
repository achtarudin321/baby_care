@extends('layout.app')
@section('css')

@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Tambah Karyawan/Pengguna Baru</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    {{-- Alert success--}}
                    @if (session('success'))
                        @include('ui.alert.alert_success', ['message' => session('success')])
                    @endif

                    {{-- Alert error--}}
                    @if (session('error'))
                        @include('ui.alert.alert_danger', ['message' => session('error')])
                    @endif

                    {{-- Start Form --}}
                    <form action="/employee" method="POST" autocomplete="off">
                        <div class="row mt-3 mx-5">

                            @csrf

                            <div class="col-md-6">
                                {{-- First Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Depan',
                                    'id' => 'first_name',
                                    'name' => 'first_name',
                                    'placeholder' => 'Nama Depan',
                                ])

                                {{-- Middle Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Tengah',
                                    'id' => 'middle_name',
                                    'name' => 'middle_name',
                                    'placeholder' => 'Nama Tengah',
                                ])

                                {{-- Last Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Belakang',
                                    'id' => 'last_name',
                                    'name' => 'last_name',
                                    'placeholder' => 'Nama Belakang',
                                ])

                                {{-- Branch --}}
                                @include('ui.input.select_tag', [
                                    'label' => 'Kantor Cabang',
                                    'id' => 'branch_id',
                                    'name' => 'branch_id',
                                    'options' => $branch
                                ])

                                {{-- Roles User --}}
                                @include('ui.input.select_tag', [
                                    'label' => 'Role / Peran',
                                    'id' => 'role_id',
                                    'name' => 'role_id',
                                    'options' => $roles
                                ])

                                {{-- Address --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Alamat',
                                    'id' => 'address',
                                    'name' => 'address',
                                    'placeholder' => 'Alamat'
                                ])
                            </div>

                            <div class="col-md-6">
                                {{-- Username --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Username',
                                    'id' => 'username',
                                    'name' => 'username',
                                    'placeholder' => 'Username',
                                ])

                                {{-- Email --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Email',
                                    'type' => 'email',
                                    'id' => 'email',
                                    'name' => 'email',
                                    'placeholder' => 'Email',
                                ])

                                {{-- Password --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Password',
                                    'type' => 'password',
                                    'id' => 'password',
                                    'name' => 'password',
                                    'placeholder' => 'Password',
                                ])

                                {{-- Password Confirmation--}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Konfrimasi Password',
                                    'type' => 'password',
                                    'id' => 'password_confirmation',
                                    'name' => 'password_confirmation',
                                    'placeholder' => 'Password',
                                ])

                                {{-- Deskripsi --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Deskripsi',
                                    'id' => 'description',
                                    'name' => 'description',
                                    'placeholder' => 'Deskripsi'
                                ])

                                <div class="float-right mt-5">
                                    <button class="btn btn-info" type="submit">Kirim</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    {{-- End Form --}}

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
