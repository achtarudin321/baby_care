@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="card-title">
                <i class="fas fa-sitemap"></i> Edit Informasi Karyawan/Pengguna
            </h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header bg-success">
                        <h4 class="m-b-0 text-white">Nama : {{$userTenant->complete_name}}</h4>
                </div>

                {{-- Alert success--}}
                @if (session('success'))
                    @include('ui.alert.alert_success', ['message' => session('success')])
                @endif

                {{-- Alert error--}}
                @if (session('error'))
                    @include('ui.alert.alert_danger', ['message' => session('error')])
                @endif

                <div class="card-body">
                    <form action="/employee/{{$userTenant->user_id}}" method="POST">
                        <div class="row mt-3 mx-5">

                            @csrf
                            @method('PUT')

                            <div class="col-md-6">
                                {{-- First Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Depan',
                                    'id' => 'first_name',
                                    'name' => 'first_name',
                                    'placeholder' => 'Nama Depan',
                                    'value' => $userTenant->user_master->first_name
                                ])

                                {{-- Middle Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Tengah',
                                    'id' => 'middle_name',
                                    'name' => 'middle_name',
                                    'placeholder' => 'Nama Tengah',
                                    'value' => $userTenant->user_master->middle_name
                                ])

                                {{-- Middle Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Tengah',
                                    'id' => 'middle_name',
                                    'name' => 'middle_name',
                                    'placeholder' => 'Nama Tengah',
                                    'value' => $userTenant->user_master->last_name
                                ])

                                {{-- Address --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Alamat',
                                    'id' => 'address',
                                    'name' => 'address',
                                    'placeholder' => 'Alamat',
                                    'value' => $userTenant->address
                                ])

                                {{-- Branch --}}
                                @include('ui.input.select_tag', [
                                    'label' => 'Kantor Cabang',
                                    'id' => 'branch_id',
                                    'name' => 'branch_id',
                                    'options' => $branch,
                                    'selected' => $userTenant->branch_id
                                ])

                            </div>

                            <div class="col-md-6">
                                {{-- Username --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Username',
                                    'id' => 'username',
                                    'name' => 'username',
                                    'placeholder' => 'Username',
                                    'readonly' => 'readonly',
                                    'value' => $userTenant->user_master->username
                                ])

                                {{-- Email --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Username',
                                    'id' => 'username',
                                    'name' => 'username',
                                    'placeholder' => 'Username',
                                    'readonly' => 'readonly',
                                    'value' => $userTenant->user_master->email
                                ])

                                @foreach ($userTenant->user_master->roles as $role)
                                    @php
                                        $role = $role->id
                                    @endphp
                                @endforeach

                                {{-- Roles User --}}
                                @include('ui.input.select_tag', [
                                    'label' => 'Role / Peran',
                                    'id' => 'role_id',
                                    'name' => 'role_id',
                                    'options' => $roles,
                                    'selected' => $role ?? null
                                ])

                                {{-- Deskripsi --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Deskripsi',
                                    'id' => 'description',
                                    'name' => 'description',
                                    'placeholder' => 'Deskripsi',
                                    'value' => $userTenant->user_master->description_user->description
                                ])

                                <div class="float-right mt-5">
                                    <button class="btn btn-info" type="submit">Kirim</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
