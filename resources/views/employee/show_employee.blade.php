@extends('layout.app')

@section('css')

@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="card-title"><i class="fas fa-sitemap"></i> Nama : {{$userTenant->complete_name}}</h4>
        </div>

        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a class="btn btn-info d-none d-lg-block m-l-15"
                href="/employee/{{$userTenant->user_id}}/edit"><i class="fa fa-plus-circle"></i> Ubah </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">Info Karyawan/Pengguna</h4>
                </div>

                <div class="card-body">
                    <div class="mx-5">
                        <div class="row">

                            <div class="col-md-6">
                                @include('employee.ui_show_employee', [
                                    'label' => 'Nama',
                                    'data' => $userTenant->complete_name
                                ])

                                @include('employee.ui_show_employee', [
                                    'label' => 'Gender',
                                    'data' => $userTenant->gender
                                ])

                                @include('employee.ui_show_employee', [
                                    'label' => 'Telephone',
                                    'data' => $userTenant->phone
                                ])

                                @include('employee.ui_show_employee', [
                                    'label' => 'Alamat',
                                    'data' => $userTenant->address
                                ])

                                @include('employee.ui_show_employee', [
                                    'label' => 'Cabang',
                                    'data' => $userTenant->branch->name
                                ])
                            </div>

                            <div class="col-md-6">
                                @include('employee.ui_show_employee', [
                                    'label' => 'Email',
                                    'data' => $userTenant->user_master->email
                                ])

                                @include('employee.ui_show_employee', [
                                    'label' => 'Username',
                                    'data' => $userTenant->user_master->username
                                ])

                                @include('employee.ui_show_employee', [
                                    'label' => 'Deskripsi',
                                    'data' => $userTenant->user_master->description_user->description
                                ])

                                @include('employee.ui_show_employee', [
                                    'label' => 'Status',
                                    'data' => $userTenant->user_master->description_user->is_active ? 'Aktif' : 'Tidak Aktif'
                                ])

                                @foreach ($userTenant->user_master->roles as $role)
                                    @php
                                        $role = $role->name
                                    @endphp
                                @endforeach

                                @include('employee.ui_show_employee', [
                                    'label' => 'Role / Peran',
                                    'data' => $role ?? '-'
                                ])
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
