@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="card-title"><i class="fas fa-sitemap"></i> Tenant Nama : {{$tenant->name}}</h4>
        </div>

        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a class="btn btn-info d-none d-lg-block m-l-15"
                href="/tenant/{{$tenant->id}}/edit"><i class="fa fa-plus-circle"></i> Ubah </a>
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">Info Tenant</h4>
                    </div>

                    <div class="card-body">

                        <div class="mx-5">
                            <div class="row">
                                <div class="col-md-6">

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Kode',
                                        'data' => $tenant->code
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Nama',
                                        'data' => $tenant->name
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Pemilik',
                                        'data' => $tenant->owner
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Npwp',
                                        'data' => $tenant->npwp
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Fax',
                                        'data' => $tenant->fax
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Telephone',
                                        'data' => $tenant->phone
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Email',
                                        'data' => $tenant->email
                                    ])

                                </div>

                                <div class="col-md-6">

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Alamat',
                                        'data' => $tenant->address
                                    ])


                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Deskripsi',
                                        'data' => $tenant->description
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Host',
                                        'data' => $tenant->db_host
                                    ])


                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Port',
                                        'data' => $tenant->db_port
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Database',
                                        'data' => $tenant->db_name
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Username',
                                        'data' => $tenant->db_username
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Password',
                                        'data' => '************'
                                    ])

                                    @include('tenant.ui_show_tenant', [
                                        'label' => 'Status',
                                        'data' => $tenant->is_active ? 'Tenant Aktif' : "Tenant Tidak Aktif"
                                    ])
                                </div>

                                <div class="col-md-4">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

@endsection

@section('js')
@endsection

