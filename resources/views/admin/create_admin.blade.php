@extends('layout.app')

@section('css')

@endsection

@section('content')
    <div class="row page-titles">

        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Tambah Admin/Penyewa Baru</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    @if (session('error'))
                        @include('ui.alert.alert_danger', [
                            'message' => session('error')
                        ])
                    @endif

                    <form action="/admin/store" method="POST" autocomplete="off">

                        <div class="row justify-content-center mt-5">

                            @csrf

                            <div class="col-md-3">

                                {{-- First Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Depan',
                                    'id' => 'first_name',
                                    'name' => 'first_name',
                                    'placeholder' => 'Nama Depan',
                                ])

                                {{-- Middle Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Tengah',
                                    'id' => 'middle_name',
                                    'name' => 'middle_name',
                                    'placeholder' => 'Nama Tengah',
                                ])

                                {{-- Last Name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Belakang',
                                    'id' => 'last_name',
                                    'name' => 'last_name',
                                    'placeholder' => 'Nama Belakang',
                                ])

                                {{-- Tenant --}}
                                @include('ui.input.select_tag', [
                                    'label' => 'Nama Tenant',
                                    'id' => 'tenant_id',
                                    'name' => 'tenant_id',
                                    'options' => $tenant
                                ])

                                {{-- Address --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Alamat',
                                    'id' => 'address',
                                    'name' => 'address',
                                    'placeholder' => 'Alamat'
                                ])

                            </div>

                            <div class="col-md-3">

                                {{-- Username --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Username',
                                    'id' => 'username',
                                    'name' => 'username',
                                    'placeholder' => 'Username',
                                ])

                                {{-- Email --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Email',
                                    'type' => 'email',
                                    'id' => 'email',
                                    'name' => 'email',
                                    'placeholder' => 'Email',
                                ])

                                {{-- Password --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Password',
                                    'type' => 'password',
                                    'id' => 'password',
                                    'name' => 'password',
                                    'placeholder' => 'Password',
                                ])

                                {{-- Password Confirmation--}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Konfrimasi Password',
                                    'type' => 'password',
                                    'id' => 'password_confirmation',
                                    'name' => 'password_confirmation',
                                    'placeholder' => 'Password',
                                ])

                                {{-- Deskripsi --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Deskripsi',
                                    'id' => 'description',
                                    'name' => 'description',
                                    'placeholder' => 'Deskripsi'
                                ])

                                <div class="float-right mt-2">
                                    <button class="btn btn-info" type="submit">Kirim</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection
