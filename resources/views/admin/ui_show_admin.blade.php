<div class="form-group row">
    <label class="control-label text-right col-md-4">
        <div class="float-left">
            <h5 class="card-title">{{$label}} :</h5>
        </div>
    </label>
    <div class="col-md-8">
        <h5 class="card-title form-control-static">
            <span class=" py-1 px-3 border border-success rounded">{{$data ?? '-'}}</span>
        </h5>
    </div>
</div>
