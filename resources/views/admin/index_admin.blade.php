@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">

        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">List Admin Penyewa/Tenant</h4>
        </div>

        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a class="btn btn-info d-none d-lg-block m-l-15"
                href="admin/create"><i class="fa fa-plus-circle"></i> Tambah Admin Penyewa Baru</a>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    @if (session('success'))
                        @include('ui.alert.alert_success', ['message' => session('success')])
                    @endif

                    @if (session('error'))
                        @include('ui.alert.alert_danger', ['message' => session('error')])
                    @endif

                    <h4 class="card-title">Daftar Admin Penyewa/Tenant</h4>

                    <div class="mt-5 table-responsive">
                        <table class="table table-bordered table-hover">

                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Deskripsi</th>
                                    <th>Nama Tenant</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>

                            <tbody>
                                @forelse ($userAdmin as $user)
                                    @if ($user->hasAccess(['user.tenant','user.admin.tenant']))
                                        <tr>
                                            <td class="text-center">{{$user->id}}</td>
                                            <td>{{$user->full_name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->description_user->description}}</td>
                                            <td>{{$user->description_user->tenant_master->name}}</td>
                                            <td class="text-center">
                                                <a href="/admin/{{$user->email}}" class="btn btn-info btn-sm">
                                                    <i class="fas fa-info-circle"></i> Lihat
                                                </a>

                                                @if ($user->description_user->is_active)

                                                    <a href="/admin-disabled/{{$user->email}}" class="btn btn-danger btn-sm">
                                                        <i class="fas fa-ban"></i> Non-Aktifkan
                                                    </a>

                                                @else

                                                    <a href="/admin-enabled/{{$user->email}}" class="btn btn-success btn-sm">
                                                        <i class="fas fa-photo"></i> Aktifkan
                                                    </a>

                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @empty
                                <tr>
                                    <td colspan="7" class="text-center">
                                        Belum Ada Admin Tersedia
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>

                        </table>
                    </div>

                    <div class="float-right">
                        {{$userAdmin->links()}}
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var result = @json($userAdmin);
        console.log(result);

    </script>
@endsection
