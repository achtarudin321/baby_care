<nav class="sidebar-nav">

    <ul id="sidebarnav">
        <li class="user-pro">
            <a class="waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                <img src="{{url('/assets')}}/images/users/4.jpg" alt="user-img" class="img-circle">
                <span class="hide-menu font-weight-bold">{{Str::title($userLogin->first_name ?? '')}}</span>
            </a>
        </li>
    </ul>

    <ul>
        <li class="nav-small-cap"></li>

        <li>
            <a class="waves-effect waves-dark" href="/dashboard">
                <i class="icon-speedometer"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>

        <li>
            <a class="waves-effect waves-dark" href="/tenant">
                <i class="ti-layout-grid2"></i>
                <span class="hide-menu">Tenant</span>
            </a>
        </li>

        <li>
            <a class="waves-effect waves-dark" href="/admin">
                <i class="ti-palette"></i>
                <span class="hide-menu">Admin</span>
            </a>
        </li>

        <li class="nav-small-cap"></li>
        <li class="nav-small-cap"></li>
    </ul>
</nav>
