<nav class="sidebar-nav">

    <ul id="sidebarnav">

        <li class="user-pro">
            <a class="text-center waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                <img src="{{url('/assets')}}/images/users/4.jpg" alt="user-img" class="img-circle"><br>
            </a>
            <p class="text-info font-weight-bold text-center">{{Str::title($userLogin->user_tenant->complete_name ?? '')}}</p>
        </li>

        <li class="nav-small-cap"></li>

        @forelse ($menuTenant as $menus)

            @if ($menus->menu_children->count() > 0)

                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="ti-layout-media-right-alt"></i>
                        <span class="hide-menu">{{$menus->name}}</span>
                    </a>

                    <ul aria-expanded="false" class="collapse">
                        @foreach ($menus->menu_children->sortBy('sort') as $menuChild)
                            <li><a href="{{$menuChild->url}}">{{$menuChild->name}}</a></li>
                        @endforeach
                    </ul>
                </li>

            @else

                <li>
                    <a class="waves-effect waves-dark" href="{{$menus->url}}">
                        <i class="{{$menus->icon}}"></i>
                        <span class="hide-menu">{{$menus->name}}</span>
                    </a>
                </li>

            @endif
        @empty
            <li>
                <a class="waves-effect waves-dark" href="">
                    <i class=""></i>
                    <span class="hide-menu">Belum ada Route</span>
                </a>
            </li>
        @endforelse
        <li class="nav-small-cap"></li>
    </ul>
    </nav>
