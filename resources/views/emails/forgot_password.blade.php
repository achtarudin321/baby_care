@extends('auth.layout.layout_auth')

@section('content')
<h3 class="text-center">Hello {{$user->first_name ?? ''}}</h3>

<p> Untuk mengganti password silakan klik link dibawah</p>

<a class="btn btn-primary"
    href="{{url('/')}}reset-password/{{ $user->email }}/{{ $code}}">
    Activate your account
</a>

<p>{{url('/')}}/reset-password/{{ $user->email }}/{{ $code}}</p>
@endsection
