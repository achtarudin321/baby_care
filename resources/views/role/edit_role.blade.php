@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Edit Role / Peran</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    {{-- Alert success--}}
                    @if (session('success'))
                        @include('ui.alert.alert_success', ['message' => session('success')])
                    @endif

                    {{-- Alert error--}}
                    @if (session('error'))
                        @include('ui.alert.alert_success', ['message' => session('error')])
                    @endif

                    <form action="/roles/{{$tenantRole->id}}" method="post">
                        <div class="mt-3 mx-5">
                            @csrf

                            @method('PUT')

                            @include('ui.input.input_tag', [
                                'label' => 'Nama Role',
                                'id' => 'role-input',
                                'placeholder' => 'Roles Baru',
                                'name' => 'roleName',
                                'value' => $tenantRole->name
                            ])

                            <table class="table table-bordered table-hover">

                                <thead>
                                    <tr>

                                        <th>Nama Halaman</th>
                                        <th class="text-center">Create</th>
                                        <th class="text-center">Read</th>
                                        <th class="text-center">Update</th>
                                        <th class="text-center">Delete</th>
                                        <th class="text-center">Export</th>
                                        <th class="text-center">Import</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @forelse ($menus as $menu)
                                        @if ($menu->menu_children->count() > 0)

                                            <tr>
                                                <td colspan="7" class="text-left mute">{{$menu->name}}</td>
                                            </tr>

                                            @foreach ($menu->menu_children->sortBy('sort') as $key => $menuChild)

                                                <tr>
                                                    <td>{{$menuChild->name}}</td>

                                                    <td class="text-center">
                                                        @include('ui.input.checkbox_tag', [
                                                            'name' => "checked[create-{$menuChild->code}]",
                                                            'id' => "create-{$menuChild->code}",
                                                            'for' => "create-{$menuChild->code}",
                                                            'checked' => $tenantRole->permissions["create-{$menuChild->code}"] ?? null
                                                        ])
                                                    </td>

                                                    <td class="text-center">
                                                        @include('ui.input.checkbox_tag', [
                                                            'name' => "checked[read-{$menuChild->code}]",
                                                            'id' => "read-{$menuChild->code}",
                                                            'for' => "read-{$menuChild->code}",
                                                            'checked' => $tenantRole->permissions["read-{$menuChild->code}"] ?? null
                                                        ])
                                                    </td>

                                                    <td class="text-center">
                                                        @include('ui.input.checkbox_tag', [
                                                            'name' => "checked[update-{$menuChild->code}]",
                                                            'id' => "update-{$menuChild->code}",
                                                            'for' => "update-{$menuChild->code}",
                                                            'checked' => $tenantRole->permissions["update-{$menuChild->code}"] ?? null
                                                        ])
                                                    </td>

                                                    <td class="text-center">
                                                        @include('ui.input.checkbox_tag', [
                                                            'name' => "checked[delete-{$menuChild->code}]",
                                                            'id' => "delete-{$menuChild->code}",
                                                            'for' => "delete-{$menuChild->code}",
                                                            'checked' => $tenantRole->permissions["delete-{$menuChild->code}"] ?? null
                                                        ])
                                                    </td>

                                                    <td class="text-center">
                                                        @include('ui.input.checkbox_tag', [
                                                            'name' => "checked[export-{$menuChild->code}]",
                                                            'id' => "export-{$menuChild->code}",
                                                            'for' => "export-{$menuChild->code}",
                                                            'checked' => $tenantRole->permissions["export-{$menuChild->code}"] ?? null
                                                        ])
                                                    </td>

                                                    <td class="text-center">
                                                        @include('ui.input.checkbox_tag', [
                                                            'name' => "checked[import-{$menuChild->code}]",
                                                            'id' => "import-{$menuChild->code}",
                                                            'for' => "import-{$menuChild->code}",
                                                            'checked' => $tenantRole->permissions["import-{$menuChild->code}"] ?? null
                                                        ])
                                                    </td>
                                                </tr>
                                            @endforeach

                                        @else
                                            <tr>
                                                <td>{{$menu->name}}</td>
                                                {{-- Checkbox Create --}}
                                                <td class="text-center">
                                                    @include('ui.input.checkbox_tag', [
                                                        'name' => "checked[create-{$menu->code}]",
                                                        'id' => "create-{$menu->code}",
                                                        'for' => "create-{$menu->code}",
                                                        'checked' => $tenantRole->permissions["create-{$menu->code}"] ?? null
                                                    ])
                                                </td>

                                                {{-- Checkbox Read --}}
                                                <td class="text-center">
                                                    @include('ui.input.checkbox_tag', [
                                                        'name' => "checked[read-{$menu->code}]",
                                                        'id' => "read-{$menu->code}",
                                                        'for' => "read-{$menu->code}",
                                                        'checked' => $tenantRole->permissions["read-{$menu->code}"] ?? null

                                                    ])
                                                </td>

                                                {{-- Checkbox Update --}}
                                                <td class="text-center">
                                                    @include('ui.input.checkbox_tag', [
                                                        'name' => "checked[update-{$menu->code}]",
                                                        'id' => "update-{$menu->code}",
                                                        'for' => "update-{$menu->code}",
                                                        'checked' => $tenantRole->permissions["update-{$menu->code}"] ?? null

                                                    ])
                                                </td>

                                                {{-- Checkbox Delete --}}
                                                <td class="text-center">
                                                    @include('ui.input.checkbox_tag', [
                                                        'name' => "checked[delete-{$menu->code}]",
                                                        'id' => "delete-{$menu->code}",
                                                        'for' => "delete-{$menu->code}",
                                                        'checked' => $tenantRole->permissions["delete-{$menu->code}"] ?? null

                                                    ])
                                                </td>

                                                {{-- Checkbox Export --}}
                                                <td class="text-center">
                                                    @include('ui.input.checkbox_tag', [
                                                        'name' => "checked[export-{$menu->code}]",
                                                        'id' => "export-{$menu->code}",
                                                        'for' => "export-{$menu->code}",
                                                        'checked' => $tenantRole->permissions["export-{$menu->code}"] ?? null
                                                    ])
                                                </td>

                                                {{-- Checkbox Import --}}
                                                <td class="text-center">
                                                    @include('ui.input.checkbox_tag', [
                                                        'name' => "checked[import-{$menu->code}]",
                                                        'id' => "import-{$menu->code}",
                                                        'for' => "import-{$menu->code}",
                                                        'checked' => $tenantRole->permissions["import-{$menu->code}"] ?? null
                                                    ])
                                                </td>
                                            </tr>
                                        @endif

                                    @empty
                                        <tr>
                                            <td colspan="7">Tidak Ada Menu</td>
                                        </tr>
                                    @endforelse

                                </tbody>
                            </table>

                            <div class="float-right">
                                <input type="submit" value="Kirim" class="btn btn-primary">
                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
