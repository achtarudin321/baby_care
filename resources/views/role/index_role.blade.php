@extends('layout.app')
@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Hak Akses Role/Peran</h4>
        </div>

        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a class="btn btn-info d-none d-lg-block m-l-15"
                href="/roles/create"><i class="fa fa-plus-circle"></i> Tambah Role / Peran Baru</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    {{-- Alert success--}}
                    @if (session('success'))
                        @include('ui.alert.alert_success', ['message' => session('success')])
                    @endif

                    {{-- Alert error--}}
                    @if (session('error'))
                        @include('ui.alert.alert_danger', ['message' => session('error')])
                    @endif

                    <h4 class="card-title"> List Role / Peran </h4>

                    <div class="table-responsive mt-5">
                        <table class="table table-bordered table-hover">

                            <thead>
                                <tr class="text-center">
                                    <th>Nama Roles</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($allRoles as $role)
                                    <tr class="text-center">
                                        <td> <a href="/roles/{{$role->id}}/edit" class="text-decoration-none text-body">{{$role->name}}</a></td>
                                        <td>
                                            @if ($role->is_active)
                                                Roles Active
                                            @else
                                                Tidak Active
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2">Belum ada roles / peran</td>
                                    </tr>
                                @endforelse
                            </tbody>

                        </table>
                    </div>

                    <div class="float-right">
                        {{$allRoles->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
