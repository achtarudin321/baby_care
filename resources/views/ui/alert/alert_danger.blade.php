<div class="row justify-content-center">
    <div class="col-md-5">
        <div class="alert alert-rounded alert-danger alert-dismissible fade show" role="alert">
        <strong>{{$message ?? 'Terjadi Kesalahan Silakan Coba Lagi'}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>

