<div class="row justify-content-center">
        <div class="col-md-4">
            <div class="alert alert-rounded alert-danger alert-dismissible fade show" role="alert">
            <strong>Akun Tidak Tersedia</strong>  .
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>

