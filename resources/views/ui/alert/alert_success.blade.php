<div class="row justify-content-center">
    <div class="col-md-5">
        <div class="alert alert-rounded alert-success alert-dismissible fade show" role="alert">
        <strong>{{$message ?? 'Berhasil di buat'}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>

