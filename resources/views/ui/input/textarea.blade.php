<div class="form-group">
    <label>{{$label}}</label>
    <textarea name="{{$name}}" placeholder="{{$placeholder}}"
        id="{{$id}}" class="form-control">{{$value ?? '' ?? old($name)}}</textarea>
    @if ($errors->first($name))
        <small class="form-control-feedback text-danger"> {{$errors->first($name)}} </small>
    @endif
</div>
