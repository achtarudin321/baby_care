<div class="form-group">
    <label>{{$label}}</label>
    <input type="{{$type ?? 'text'}}" class="form-control" {{$readonly ?? ''}}
    id="{{$id}}" placeholder="{{$placeholder}}" name="{{$name}}" value="{{ $value ?? old($name)}}">
    @if ($errors->first($name))
        <small class="form-control-feedback text-danger"> {{$errors->first($name)}} </small>
    @endif
</div>
