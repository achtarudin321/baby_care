@php
@endphp
<div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input"
        name="{{$name}}" id="{{$id}}" value="true" {{ isset($checked) ?'checked' : ''}}>
    <label class="custom-control-label" for="{{$for}}"></label>
</div>
