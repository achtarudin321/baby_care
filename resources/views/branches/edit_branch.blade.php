@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="card-title">
                <i class="fas fa-sitemap"></i> Edit Kantor Cabang : {{$branch->name}}
            </h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header bg-success">
                    <h4 class="m-b-0 text-white">Kantor Cabang : {{$branch->name}}</h4>
                </div>

                {{-- Alert success--}}
                @if (session('success'))
                    @include('ui.alert.alert_success', ['message' => session('success')])
                @endif

                {{-- Alert error--}}
                @if (session('error'))
                    @include('ui.alert.alert_danger', ['message' => session('error')])
                @endif

                <div class="card-body">
                    <form action="/branches/{{$branch->email}}" method="POST" autocomplete="off">
                        <div class="row mt-3 mx-5">
                            @csrf
                            @method('PUT')

                            <div class="col-md-6">
                                {{-- Code --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Kode Cabang',
                                    'id' => 'code',
                                    'name' => 'code',
                                    'value' => $branch->code,
                                    'readonly' => 'readonly',
                                    'placeholder' => 'Kode Cabang',
                                ])

                                {{-- Branch--}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Nama Cabang',
                                    'id' => 'name',
                                    'name' => 'name',
                                    'value' => $branch->name,
                                    'placeholder' => 'Nama Cabang'
                                ])

                                {{-- Owner name --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Pemilik / Penanggung Jawab',
                                    'id' => 'owner_name',
                                    'name' => 'owner_name',
                                    'value' => $branch->owner_name,
                                    'placeholder' => 'Pemilik / Penanggung Jawab'
                                ])

                                {{-- Address --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Alamat',
                                    'id' => 'address',
                                    'name' => 'address',
                                    'value' => $branch->address,
                                    'placeholder' => 'Alamat'
                                ])

                            </div>

                            <div class="col-md-6">
                                {{-- Telephone --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Telephone',
                                    'id' => 'phone',
                                    'name' => 'phone',
                                    'value' => $branch->phone,
                                    'placeholder' => 'Telephone'
                                ])

                                {{-- Fax --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Fax',
                                    'id' => 'fax',
                                    'name' => 'fax',
                                    'value' => $branch->fax,
                                    'placeholder' => 'Fax'
                                ])

                                {{-- Email --}}
                                @include('ui.input.input_tag', [
                                    'label' => 'Email',
                                    'type' => 'email',
                                    'id' => 'email',
                                    'name' => 'email',
                                    'value' => $branch->email,
                                    'readonly' => 'readonly',
                                    'placeholder' => 'Email'
                                ])

                                {{-- Deskripsi --}}
                                @include('ui.input.textarea', [
                                    'label' => 'Deskripsi',
                                    'id' => 'description',
                                    'name' => 'description',
                                    'value' => $branch->description,
                                    'placeholder' => 'Deskripsi'
                                ])

                                <div class="float-right">
                                    <button class="btn btn-info" type="submit">Kirim</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
