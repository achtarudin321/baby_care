@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Kantor Cabang</h4>
        </div>

        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a class="btn btn-info d-none d-lg-block m-l-15"
                href="/branches/create"><i class="fa fa-plus-circle"></i> Tambah Cabang Baru</a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    {{-- Alert success--}}
                    @if (session('success'))
                        @include('ui.alert.alert_success', ['message' => session('success')])
                    @endif

                    {{-- Alert error--}}
                    @if (session('error'))
                        @include('ui.alert.alert_danger', ['message' => session('error')])
                    @endif

                    <h4 class="card-title"> List Kantor Cabang</h4>

                    <div class="table-responsive mt-5">
                        <table class="table table-bordered table-hover">

                            <thead>
                                <tr>
                                    <th class="text-center">Kode</th>
                                    <th>Nama Cabang</th>
                                    <th>Pemilik</th>
                                    <th>Telephone</th>
                                    <th>Email</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>

                            <tbody>
                                @forelse ($allBranches as $item)

                                    <tr>
                                        <td class="text-center">{{$item->code}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->owner_name}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->email}}</td>
                                        <td class="text-center">
                                            <a href="/branches/{{$item->email}}" class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i> Lihat </a>
                                            @if ($item->status)
                                                <a href="/branches-disabled/{{$item->email}}" class="btn btn-danger btn-sm"><i class="fas fa-ban"></i> Non-Aktifkan </a>
                                            @else
                                                <a href="/branches-enabled/{{$item->email}}" class="btn btn-success btn-sm"><i class="fas fa-photo"></i> Aktifkan </a>
                                            @endif
                                        </td>
                                    </tr>

                                @empty
                                    <tr>
                                        <td colspan="7" class="text-center">
                                            Tidak Ada Kantor Cabang
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>

                        </table>
                    </div>

                    <div class="float-right">
                        {{$allBranches->links() ?? ''}}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection
