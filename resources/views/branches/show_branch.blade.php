@extends('layout.app')

@section('css')

@endsection

@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="card-title"><i class="fas fa-sitemap"></i> Nama Kantor Cabang : {{$branch->name}}</h4>
        </div>

        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a class="btn btn-info d-none d-lg-block m-l-15"
                href="/branches/{{$branch->email}}/edit"><i class="fa fa-plus-circle"></i> Ubah </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-header bg-success">
                    <h4 class="m-b-0 text-white">Info Kantor Cabang</h4>
                </div>

                <div class="card-body">

                    {{-- Alert success--}}
                    @if (session('success'))
                        @include('ui.alert.alert_success', ['message' => session('success')])
                    @endif

                    {{-- Alert error--}}
                    @if (session('error'))
                        @include('ui.alert.alert_danger', ['message' => session('error')])
                    @endif

                    <div class="mx-5">
                        <div class="row mt-5">

                            <div class="col-md-6">
                                @include('branches.ui_show_branch', [
                                    'label' => 'Kode',
                                    'data' => $branch->code
                                ])

                                @include('branches.ui_show_branch', [
                                    'label' => 'Nama Cabang',
                                    'data' => $branch->name
                                ])

                                @include('branches.ui_show_branch', [
                                    'label' => 'Pemilik',
                                    'data' => $branch->owner_name
                                ])

                                @include('branches.ui_show_branch', [
                                    'label' => 'Alamat',
                                    'data' => $branch->address
                                ])
                            </div>

                            <div class="col-md-6">
                                @include('branches.ui_show_branch', [
                                    'label' => 'Telephone',
                                    'data' => $branch->phone
                                ])

                                @include('branches.ui_show_branch', [
                                    'label' => 'Fax',
                                    'data' => $branch->fax
                                ])

                                @include('branches.ui_show_branch', [
                                    'label' => 'Email',
                                    'data' => $branch->email
                                ])

                                @include('branches.ui_show_branch', [
                                    'label' => 'Deskripsi',
                                    'data' => $branch->description
                                ])

                                @include('branches.ui_show_branch', [
                                    'label' => 'Status',
                                    'data' => $branch->status ? 'Aktif':'Tidak Aktif'
                                ])

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
