@extends('layout.app')

@section('css')

@endsection

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Setting Akun</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    @if (session('success'))
                        @include('ui.alert.alert_success', ['message' => session('success')])
                    @endif

                    @if (session('error'))
                        @include('ui.alert.alert_danger', ['message' => session('error')])
                    @endif

                    <div class="row px-2">
                        <div class="col-md-4 p-3 border">
                            <h3 class="text-center">Rubah password</h3>
                            <form action="/account/change-password" method="POST">
                                <br>
                                @csrf
                                {{-- password --}}
                                @include('auth.UI.tag_input', [
                                    'label' => 'Password Lama',
                                    'placeholder' => 'Password Lama',
                                    'icon' => 'ti-lock',
                                    'name' => 'old_password',
                                    'type' => 'password'
                                ])

                                {{-- password --}}
                                @include('auth.UI.tag_input', [
                                    'label' => 'Password Baru',
                                    'placeholder' => 'Password Baru',
                                    'icon' => 'ti-lock',
                                    'name' => 'new_password',
                                    'type' => 'password'
                                ])

                                {{-- password --}}
                                @include('auth.UI.tag_input', [
                                    'label' => 'Konfirmasi Password Baru',
                                    'placeholder' => 'Konfirmasi Password Baru',
                                    'icon' => 'ti-lock',
                                    'name' => 'new_password_confirmation',
                                    'type' => 'password'
                                ])
                            <div class="float-right">
                                <button type="submit" class="btn btn-info">Ganti Password</button>
                            </div>
                            </form>

                        </div>

                        <div class="col-md-4 p-3 border">
                            welcome
                        </div>

                        <div class="col-md-4 p-3 border ">
                            welcome
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection
