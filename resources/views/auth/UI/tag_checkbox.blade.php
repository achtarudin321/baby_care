<div class="form-group">
    <label class="custom-control custom-checkbox m-b-0">
        <input type="checkbox" class="custom-control-input" name="{{$name}}">
        <span class="custom-control-label">{{$label}}</span>
    </label>
</div>
