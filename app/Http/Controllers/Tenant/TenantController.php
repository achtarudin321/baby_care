<?php

namespace App\Http\Controllers\Tenant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCreateTenant;
use App\Http\Requests\UpdateTenantRequest;
use App\Models\Master\TenantMaster\TenantMasterModel;
use App\Repository\TenantRepository\TenantRepositoryInterface;

class TenantController extends Controller
{

    protected $tenantRepo;
    public function __construct(TenantRepositoryInterface $tenantRepo) {
        $this->tenantRepo = $tenantRepo;
    }

    public function index () {
        $allTenant = TenantMasterModel::paginate(10);
        return view('tenant.index_tenant', compact('allTenant'));
    }

    public function create () {
        return view('tenant.create_tenant');
    }

    public function store(TenantRepositoryInterface $tenant, StoreCreateTenant $request){
        $validated = $request->validated();
        if($validated) {
            return $tenant->createTenant($validated);
        }
    }

    public function show ($id, Request $request) {
        $tenant = TenantMasterModel::findOrFail($id);
        return view('tenant.show_tenant', compact('tenant'));
    }

    public function enableTenant($id) {
        $tenant = TenantMasterModel::findOrFail($id);
        $tenant->update(['is_active' => 1]);
        return  redirect()->back()->with('success', "Tenant Kode {$tenant->code} berhasil di Aktifkan");
    }

    public function disableTenant($id) {
        $tenant = TenantMasterModel::findOrFail($id);
        $tenant->update(['is_active' => 0]);
        return redirect()->back()->with('error', "Tenant Kode {$tenant->code} telah di Non-Aktifkan");
    }

    public function edit($id){
        $tenant = TenantMasterModel::findOrFail($id);
        return view('tenant.edit_tenant', compact('tenant'));
    }

    public function update (UpdateTenantRequest $request, $id) {

        $valid = $request->validated();
        return $this->tenantRepo->updateTenant($valid, $id);
        dd($valid, $id);
    }

}
