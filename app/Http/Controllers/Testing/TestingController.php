<?php

namespace App\Http\Controllers\Testing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel as Auth;

class TestingController extends Controller
{
    public function assignRoleToUser() {

        $bioUser =  [
            'username' => 'maliks4sss',
            'email' => 'malik@emails4sss.com',
            'password' => 'secret',
            'first_name' => 'gg',
            'middle_name' => 'mantap',
            'last_name' => 'keren',
            'permissions' => [
                'user.tenant' => true,
                'user.admin.branch' => true
            ],
        ];

        $user = Auth::registerAndActivate($bioUser);
        $role = Auth::findRoleById(3);
        $role->users()->attach($user);
        dd(Auth::getUser(), $user->id, $role->id);
    }
}
