<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PasswordUserService;
use App\Http\Requests\Password\ChangePasswordRequest;

class AccountController extends Controller
{
    protected $servicePassword;

    public function __construct(PasswordUserService $servicePassword){
        $this->servicePassword = $servicePassword;
    }
    public function index() {
        return view('account.index_account');
    }

    public function changePassword(ChangePasswordRequest $request) {
        $valid = $request->validated();
        return $this->servicePassword->changePassword($valid['old_password'], $valid['new_password']);
    }
}
