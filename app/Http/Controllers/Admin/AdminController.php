<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCreateAdmin;
use App\Models\Master\User\UserMasterModel;
use App\Models\Master\TenantMaster\TenantMasterModel;
use App\Repository\AdminRepository\AdminRepositoryInterface;

class AdminController extends Controller
{
    public function index () {
        $userAdmin =  UserMasterModel::with('description_user')->has('description_user')->paginate(10);

        return view('admin.index_admin', compact('userAdmin'));
    }

    public function create () {
        $tenant = TenantMasterModel::where('is_active', true)->get();

        return view('admin.create_admin', compact('tenant'));
    }

    public function store (StoreCreateAdmin $request, AdminRepositoryInterface $adminRepo) {
        $validate = $request->validated();

        $result;

        if($validate){
            $result = $adminRepo->createAdminTenant($validate);
        }

        if(!$result) {
            return redirect()->back()->with('error', 'Databases belum di buat');
        }
        else{
            return redirect('/admin')->with('success', "Admin dengan username {$validate['username']} berhasil di buat");
        }
    }

    public function show ($email) {
        $adminTenant = UserMasterModel::where('email', '=' ,$email)->firstOrFail();

        return view('admin.show_admin', compact('adminTenant'));
    }

    public function enableAdmin($email) {
        $adminTenant = UserMasterModel::where('email', '=' ,$email)->firstOrFail();

        $adminTenant->description_user()->update(['is_active' => true]);

        return redirect()->back()->with('success', "Admin {$adminTenant->full_name} di telah di Aktifkan");
    }

    public function disableAdmin($email) {
        $adminTenant = UserMasterModel::where('email', '=' ,$email)->firstOrFail();

        $adminTenant->description_user()->update(['is_active' => false]);

        return redirect()->back()->with('error', "Admin {$adminTenant->full_name} di Non-Aktifkan");
    }

    public function edit($email){
        $userAdmin = UserMasterModel::where('email', '=' ,$email)->firstOrFail();

        return view('admin.edit_admin', compact('userAdmin'));
    }


}
