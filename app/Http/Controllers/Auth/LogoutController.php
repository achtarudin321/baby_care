<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel as Auth;

class LogoutController extends Controller
{
    public function logoutUser () {
        Auth::logout();
        return redirect('/login');
    }
}
