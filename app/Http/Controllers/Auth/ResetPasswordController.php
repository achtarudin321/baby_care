<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PasswordUserService;
use App\Http\Requests\PostResetPasswordRequest;

class ResetPasswordController extends Controller
{
    protected $service;

    public function __construct(PasswordUserService $service) {
        $this->service =  $service;
    }

    public function resetPassword($email, $remaiderCode) {
        $userId = $this->service->resetPassword($email, $remaiderCode);
        session(['userId' => $userId]);
        return view('auth.reset_password');
    }

    public function postResetPassword(PostResetPasswordRequest $request) {

        if (!$request->session()->has('userId')) {
            abort(404);
        }

        $idUser = $request->session()->get('userId');

        $valid = $request->validated();

        return $this->service->saveNewPassword($idUser, $valid);

    }
}
