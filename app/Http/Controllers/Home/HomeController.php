<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel as Auth;

class HomeController extends Controller
{
    protected $homeTenant = '/beranda';
    protected $homeMaster = '/dashboard';
    protected $loginPage = '/login';

    public function index () {
        return $this->redirectTo(Auth::check());
    }

    protected function redirectTo($result){
        if(!$result){
            return redirect($this->loginPage);
        }

        if($result->hasAccess('user.master')){
            return redirect($this->homeMaster);
        }

        elseif($result->hasAccess('user.tenant')){
            return redirect($this->homeTenant);
        }
    }
}
