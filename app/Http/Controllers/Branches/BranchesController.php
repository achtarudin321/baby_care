<?php

namespace App\Http\Controllers\Branches;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Branch\BranchModel;
use App\Http\Requests\UpdateBranchRequest;
use App\Http\Requests\StoreCreateBranchRequest;
use App\Repository\BranchRepository\BranchRepository;

class BranchesController extends Controller
{
    protected $branchRepo;

    public function __construct (BranchRepository $branchRepo) {
        $this->middleware('admin.tenant');
        $this->branchRepo = $branchRepo;
    }

    public function index()
    {
        $allBranches = $this->branchRepo->getAllBranch();
        return view('branches.index_branch', compact('allBranches'));
    }


    public function create()
    {
        return view('branches.create_branch');
    }


    public function store(StoreCreateBranchRequest $request)
    {

        $valid = $request->validated();
        return $this->branchRepo->createBranch($valid);
    }


    public function show($email)
    {
        $branch = $this->branchRepo->findBranch($email);
        return view('branches.show_branch', compact('branch'));
    }

    public function edit($email)
    {
        $branch = $this->branchRepo->editBranch($email);
        return view('branches.edit_branch', compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBranchRequest $request, $id)
    {
        // dd($request->all());
        $valid = $request->validated();
        return $this->branchRepo->updateBranch($valid, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function enableBranch ($email) {
        return $this->branchRepo->enable($email);
    }

    public function disableBranch ($email) {
        return $this->branchRepo->disable($email);
    }
}
