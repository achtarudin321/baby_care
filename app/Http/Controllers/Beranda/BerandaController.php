<?php

namespace App\Http\Controllers\Beranda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BerandaController extends Controller
{
    public function index() {
        return view('beranda.index_beranda');
    }
}
