<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Branch\BranchModel;
use App\Models\Tenant\UserTenant\UserTenantModel;
use App\Repository\RoleRepository\RoleRepository;
use App\Http\Requests\Employee\StoreEmployeeRequest;
use App\Repository\EmployeeRepository\EmployeeRepository;

class EmployeeController extends Controller
{

    protected $repoUser;
    protected $roleRepo;

    public function __construct(EmployeeRepository $repoUser, RoleRepository $roleRepo) {
        $this->middleware('admin.tenant');
        $this->repoUser = $repoUser;
        $this->roleRepo = $roleRepo;
    }
    public function index()
    {
        $userTenant = UserTenantModel::whereNotNull('branch_id')->get();
        return view('employee.index_employee', compact('userTenant'));
    }


    public function create()
    {
        $roles = $this->roleRepo->getRolesActive();
        $branch = BranchModel::where('status', true)->get();
        return view('employee.create_employee', compact('branch', 'roles'));
    }


    public function store(StoreEmployeeRequest $request)
    {
        $valid = $request->validated();
        return $this->repoUser->createEmployee($valid);
    }


    public function show($id)
    {
        $userTenant = $this->repoUser->employeeFindById($id);
        return view('employee.show_employee', compact('userTenant'));

    }


    public function edit($id)
    {
        $userTenant = $this->repoUser->employeeFindById($id);
        $roles = $this->roleRepo->getRolesActive();
        $branch = BranchModel::all();
        return view('employee.edit_employee', compact('userTenant', 'roles', 'branch'));
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
