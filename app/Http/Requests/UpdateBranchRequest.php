<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Sentinel as Auth;

class UpdateBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser()->hasAccess('user.tenant');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => '',
            'owner_name' => 'required',
            'phone' => 'required',
            'fax' => 'required',
            'email' => 'required',
            'address' => 'required',
            'description' => 'required',
        ];
    }

    public function messages() {
        return [
            'owner_name.required' => 'Wajib di isi',
            'phone.required' => 'Telephone harap di isi',
            'fax.required' => 'Fax harap di isi',
            'email.required' => 'Email harap di isi',
            'address.required' => 'Alamat harap di isi',
            'description.required' => 'Deskripsi harap di isi',
        ];
    }
}
