<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCreateTenant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "code"  => "required|unique:ms_tenant",
            "name"  => "required",
            "npwp"  => "numeric|unique:ms_tenant",
            "owner" => "required",
            "address" => "required",
            "phone" => "numeric|unique:ms_tenant",
            "fax"   => "numeric|unique:ms_tenant",
            "email" => "required|regex:/^.+@.+$/i|unique:ms_tenant",
            "description" => "required",
            "host"  => "required",
            "port"  => "required",
            "username" => "required",
            "db_name" => "required|unique:ms_tenant",
            "pass" => ''
        ];
    }

    public function messages()
    {
        return [
            "code.required"  => "Kode tenant harap di isi",
            "code.unique"  => "Kode tenant sudah digunakan",
            "name.required"  => "Nama tenant harap di isi",
            "npwp.numeric"  => "Npwp harap di isi",
            "npwp.unique"  => "Npwp sudah digunakan",
            "owner.required" => "Penanggung Jawab harap di isi",
            "address.required" => "Alamat harap di isi",
            "phone.numeric" => "Format salah",
            "phone.unique" => "Nomer sudah digunakan",
            "fax.numeric"   => "Format salah",
            "fax.unique"   => "Nomer sudah digunakan",
            "email.required" => "Email wajib di isi",
            "email.regex" => "Format email salah",
            "email.unique" => "Email sudah digunakan",
            "description.required" => "Deskripsi harus di isi",
            "host.required"  => "Host harap di isi",
            "port.required"  => "Port harap di isi",
            "username.required" => "Username harap di isi",
            "db_name.required" => "Database wajib di isi",
            "db_name.unique" => "Database sudah di gunakan",
        ];
    }
}
