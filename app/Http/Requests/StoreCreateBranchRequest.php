<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Sentinel as Auth;

class StoreCreateBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser()->hasAccess('user.tenant');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|unique:mysql_tenant.tn_branch',
            'name' => 'required|unique:mysql_tenant.tn_branch',
            'owner_name' => 'required',
            'phone' => 'required',
            'fax' => 'required',
            'email' => 'required|unique:mysql_tenant.tn_branch',
            'address' => 'required',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Kode cabang harap di isi',
            'code.unique' => 'Kode cabang sudah digunakan',

            'name.required' => 'Nama cabang harap di isi',
            'name.unique' => 'Nama cabang sudah digunakan',

            'owner_name.required' => 'Wajib di isi',
            'phone.required' => 'Telephone harap di isi',
            'fax.required' => 'Fax harap di isi',

            'email.required' => 'Email harap di isi',
            'email.unique' => 'Email sudah digunakan',

            'address.required' => 'Alamat harap di isi',
            'description.required' => 'Deskripsi harap di isi',
        ];
    }
}
