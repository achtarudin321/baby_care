<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'confirmed|required|min:5|max:10',
            'password_confirmation' => 'required|min:5|max:10'
        ];
    }

    public function messages() {
        $minPassword = 'Password minimal 5 alphabet';
        $maxPassword = 'Password maksmal 10 alphabet';

        return [
            'password.confirmed' => 'Password tidak sama',
            'password.required' => 'Silakan masukan password',
            'password.min' => $minPassword,
            'password.max' => $maxPassword,

            'password_confirmation.required' => 'Silakan masukan password',
            'password_confirmation.min' => $minPassword,
            'password_confirmation.max' => $maxPassword,
        ];
    }
}
