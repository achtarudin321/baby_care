<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;
use Sentinel as Auth;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser()->hasAccess('user.tenant');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'middle_name' => '',
            'last_name' => 'required',
            'branch_id' => 'required|not_in:0',
            'role_id' => '',
            'address' => 'required',
            'username' => 'required|unique:ms_users',
            'email' => 'required|unique:ms_users|regex:/^.+@.+$/i',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'description' => 'required',
        ];
    }

    public function messages() {
        return [
            'first_name.required' => 'Nama depan harus di isi',
            'last_name.required' => 'Nama Belakang harus di isi',

            'branch_id.required' => 'Tenant wajib di pilih',
            'branch_id.not_in' => 'Silakan pilih tenant',
            'address.required' => 'Alamat wajib di isi',

            'username.required' => 'Username harus di isi',
            'username.unique' => 'Username sudah di gunakan',

            'email.required' => 'Email harus di isi',
            'email.regex' => 'Format email salah',
            'email.unique' => 'Email sudah digunakan',

            'password.required' => 'Password harus di isi',
            'password.confirmed' => 'Password tidak sama',
            'password_confirmation.required' => 'Konfrimasi Password harus di isi',

            'description.required' => 'Deskripsi harus di isi',
        ];
    }
}
