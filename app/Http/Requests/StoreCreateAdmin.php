<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCreateAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name" => 'required',
            "middle_name" => '',
            "last_name" => 'required',
            "address" => 'required',
            "description" => 'required',
            "username" => 'required|unique:ms_users',
            "email" => 'required|unique:ms_users|regex:/^.+@.+$/i',
            "password" => 'required|confirmed',
            "password_confirmation" => 'required',
            "tenant_id" => 'required|not_in:0',
        ];
    }

    public function messages() {
        return [
            "first_name.required" => 'Nama depan harus di isi',
            "last_name.required" => 'Nama Belakang harus di isi',
            "address.required" => 'Alamat wajib di isi',
            "description.required" => 'Deskripsi harus di isi',
            "username.required" => 'Username harus di isi',
            "username.unique" => 'Username sudah di gunakan',
            "email.required" => 'Email harus di isi',
            "email.regex" => "Format email salah",
            "email.unique" => "Email sudah digunakan",
            "password.required" => 'Password harus di isi',
            "password.confirmed" => 'Password tidak sama',
            "password_confirmation.required" => "Konfrimasi Password harus di isi",
            "tenant_id.required" => 'Tenant wajib di pilih',
            "tenant_id.not_in" => 'Silakan pilih tenant',
        ];
    }
}
