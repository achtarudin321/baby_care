<?php

namespace App\Http\Middleware\AdminTenant;

use Closure;
use Sentinel as Auth;
class AdminTenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::getUser()->hasAccess(['user.tenant', 'user.admin.tenant'])){
            abort(404);
        }
        return $next($request);
    }
}
