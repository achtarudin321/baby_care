<?php

namespace App\Http\Middleware\AuthMiddleware;

use Closure;
use View;
use Sentinel as Auth;
use App\Repository\ConnectionRepository\ConnectionRepositoryInterface;

class TenantMiddleware
{
    protected $connect;

    public function __construct(ConnectionRepositoryInterface $connect) {
        $this->connect = $connect;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()->hasAccess('user.tenant')){

            try {

                $user = Auth::getUser()->description_user;

                $result = $this->userAndTenantShouldBeActive($user);

                if(!$result) {

                    $message = "Akun anda atau Tenant sedang tidak aktif";
                    return $this->redirectToLoginWithMessage('error', $message);

                }
                else{
                    $result = $this->connect->databaseExist( $user->tenant_id);
                    return $next($request);
                }

            }
            catch (\Exception $th) {
                $this->redirectToLoginWithMessage('error', 'Opss Something Wrong in TenantMiddleware Line 47' );
            }

        }
        else {
            return abort(404);
        }
    }

    protected function userAndTenantShouldBeActive($user) {

        return $user->is_active && $user->tenant_master->is_active ?  true : false;
    }

    protected function redirectToLoginWithMessage($status, $message){
        Auth::logout();
        return redirect('/login')->with($status, $message);
    }
}
