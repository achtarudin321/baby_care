<?php

namespace App\Http\Middleware\AuthMiddleware;

use Closure;
use View;
use Sentinel as Auth;

class IsLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($result = Auth::check()){
            return $next($request);
        }
        else {
            return redirect('/login');
        }
    }
}
