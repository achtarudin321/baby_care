<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\LoginRepository\LoginRepositoryInterface;
use App\Repository\LoginRepository\LoginRepository;

class BackedServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            LoginRepositoryInterface::class,
            LoginRepository::class
        );

        $this->app->bind(
            'App\Repository\TenantRepository\TenantRepositoryInterface',
            'App\Repository\TenantRepository\TenantRepository'
        );

        $this->app->bind(
            'App\Repository\AdminRepository\AdminRepositoryInterface',
            'App\Repository\AdminRepository\AdminRepository'
        );

        $this->app->bind(
            'App\Repository\ConnectionRepository\ConnectionRepositoryInterface',
            'App\Repository\ConnectionRepository\ConnectionRepository'
        );

        $this->app->bind(
            'App\Repository\BranchRepository\BranchRepositoryInterface',
            'App\Repository\BranchRepository\BranchRepository'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
