<?php

namespace App\Providers;

use Sentinel as Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\Tenant\Menu\MenuTenantModel;
use App\Repository\ConnectionRepository\ConnectionRepositoryInterface;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    protected $connect;
    public function __construct() {
    }

    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(ConnectionRepositoryInterface $connect)
    {

        View::composer('*', function($view){
            $view->with('userLogin', Auth::getUser() ?? null);
        });

        View::composer('*', function($view) use ($connect){
            $menuTenant = [];

            if (!Auth::check() || Auth::check()->hasAccess('user.master')) {

                $view->with('menuTenant', $menuTenant);
            }

            else {
                $user = Auth::getUser()->description_user;
                $connect->databaseExist( $user->tenant_id);

                // if(Auth::getUser()->hasAccess(['user.tenant', 'user.admin.tenant'])){

                // }

                try{
                    $menuTenant = MenuTenantModel::where('is_active', true)->orderBy('sort', 'ASC')->get();

                    $view->with('menuTenant', $menuTenant ?? null);

                }
                catch(\Exception $e){

                    dd('Error On ComposerServiceProvider file line 56', $e->getMessage());

                }
            }
        });




    }
}
