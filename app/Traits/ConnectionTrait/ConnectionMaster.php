<?php
namespace App\Traits\ConnectionTrait;

trait ConnectionMaster {

    public function getConnection () {
        return $this->connection = 'mysql_master';
    }
}
