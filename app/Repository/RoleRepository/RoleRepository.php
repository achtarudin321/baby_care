<?php
namespace App\Repository\RoleRepository;

use Sentinel as Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class RoleRepository {

    protected $userAuth;
    protected $idTenant;

    public function __construct() {
    }

    public function getTenantRoles() {
        $idTenant = Auth::getUser()->tenant_id;
        $result = Auth::getRoleRepository()->where('tenant_id', $idTenant)->paginate(5);
        return $result;
    }

    public function createNewRole($newRoles) {

        $idTenant = Auth::getUser()->tenant_id;

        $roles = Arr::except($newRoles, ['roleName', '_token']);

        $permissions = [];

        foreach ($roles['checked'] as $key => $value) {
            $permissions[$key] = (bool) $value;
        }

        try{
            $unique = (string) Str::uuid();
            Auth::getUser()->createRole()->create([
                'slug' => Str::slug("{$unique} {$newRoles['roleName']}"),
                'name' => $newRoles['roleName'],
                'permissions' => $permissions,
                'tenant_id' => $idTenant,
                'is_active' => true
            ]);
            return redirect('/roles')->with('success', 'Roles / Peran baru berhasil di tambahkan');
        }
        catch(\Exception $e) {
            dd($e);
            return redirect('/roles')->with('error', 'Ops something wrong, Please try again');
        }
    }

    public function getRolesActive() {
        $idTenant = Auth::getUser()->tenant_id;
        return Auth::getRoleRepository()
            ->where([
                'tenant_id' => $idTenant,
                'is_active' => true
            ])->get();
    }

    public function getRoleById($id) {
        $idTenant = Auth::getUser()->tenant_id;
        return Auth::getRoleRepository()->where([
            'id' => $id,
            'tenant_id' => $idTenant,
            'is_active' => true
        ])->firstOrFail();
    }

    public function updateRole($id, $roleUpdate) {

        $idTenant = Auth::getUser()->tenant_id;

        $roles = Arr::except($roleUpdate, ['roleName', '_token', '_method']);

        $role = Auth::getRoleRepository()->where([
            'id' => $id,
            'tenant_id' => $idTenant,
            'is_active' => true
        ])->firstOrFail();

        $permissions = [];

        foreach ($roles['checked'] as $key => $value) {
            $permissions[$key] = (bool) $value;
        }
        $role->update([
            'name' => $roleUpdate['roleName'],
            'permissions' => $permissions
        ]);

        return redirect()->back()->with('success', 'Role Atau Peran Berhasi di Update');
    }

}
