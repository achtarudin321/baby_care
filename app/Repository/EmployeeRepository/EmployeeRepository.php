<?php
namespace App\Repository\EmployeeRepository;

use Sentinel as Auth;
use App\Models\Tenant\UserTenant\UserTenantModel;
use App\Repository\ConnectionRepository\ConnectionRepositoryInterface;

class EmployeeRepository
{
    protected $databaseTenant;

    public function __construct (ConnectionRepositoryInterface $databaseTenant) {
        $this->databaseTenant = $databaseTenant;
    }



    public function createEmployee (array $credential){

        $tenantId = Auth::getUser()->description_user->tenant_id;

        $result = $this->databaseTenant->databaseExist($tenantId);

        if(!$result){
            $message = 'Gagal Menambahkan Pengguna Baru';
            return redirecrt('/employee')->with('error', $message);
        }

        return $this->registerAndSaveToTenantAsUserBranch($credential);

    }

    protected function registerAndSaveToTenantAsUserBranch(array $credential) {

        $bioUser =  [
            'username' => $credential['username'],
            'email' => $credential['email'],
            'password' => $credential['password'],
            'first_name' => $credential['first_name'],
            'middle_name' => $credential['middle_name'] ?? '',
            'last_name' => $credential['last_name'],
            'permissions' => [
                'user.tenant' => true,
                'user.admin.branch' => true
            ],
        ];


        try{

            $user = Auth::registerAndActivate($bioUser);

            if($credential['role_id'] > 0){
                $role = Auth::findRoleById($credential['role_id']);
                $role->users()->attach($user);
            }

            // Save to description table myqsl_master
            $user->description_user()->create([
                'tenant_id' => Auth::getUser()->tenant_id,
                'description' => $credential['description'],
                'is_active' => true
            ]);

            // Save to user_tenant tanle mysql_tanant
            $user->user_tenant()->create([
                'complete_name' => $user->full_name,
                'address' =>$credential['address'],
                'branch_id' => $credential['branch_id'],
                'created_by' => Auth::getUser()->id
            ]);


            $message = "Berhasil menambahkan pengguna baru dengan username {$user->username}";
            return redirect('/employee')->with('success', $message);
        }

        catch(\Exception $e) {
            dd($e);
            return redirect('/employee')->with('error', $e->getMessage());
        }
    }

    public function employeeFindById($id){
        $userTenant = UserTenantModel::where('user_id',$id)->firstOrFail();
        return $userTenant;
    }
}
