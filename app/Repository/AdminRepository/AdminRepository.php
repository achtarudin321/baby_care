<?php
namespace App\Repository\AdminRepository;

use Illuminate\Support\Facades\DB;
use App\Models\Master\User\UserMasterModel;
use App\Models\Master\TenantMaster\TenantMasterModel;
use App\Repository\AdminRepository\AdminRepositoryInterface;
use App\Repository\ConnectionRepository\ConnectionRepositoryInterface;
use Sentinel as Auth;

class AdminRepository implements AdminRepositoryInterface {


    protected $databaseTenant;
    public function __construct (ConnectionRepositoryInterface $databaseTenant) {
        $this->databaseTenant = $databaseTenant;
    }

    public function createAdminTenant (array $credential){
        $result = $this->databaseTenant->databaseExist($credential['tenant_id']);
        if($result){
            $this->registerAndSaveToTenantUser($credential);
        }
        return $result;
    }

    protected function registerAndSaveToTenantUser (array $credential){
        $bioUser =  [
            'username' => $credential['username'],
            'email' => $credential['email'],
            'password' => $credential['password'],
            'first_name' => $credential['first_name'],
            'middle_name' => $credential['middle_name'] ?? '',
            'last_name' => $credential['last_name'],
            'permissions' => [
                'user.tenant' => true,
                'user.admin.tenant' => true
            ],
        ];

        $user = Auth::registerAndActivate($bioUser);

        $user->description_user()->create([
            'tenant_id' => $credential['tenant_id'],
            'description' => $credential['description'],
            'is_active' => true
        ]);

        $user->user_tenant()->create([
            'complete_name' => $user->full_name,
            'address' =>$credential['address'],
            'created_by' => Auth::getUser()->id
        ]);

        return true;
    }
}
