<?php
namespace App\Repository\ConnectionRepository;

use App\Models\Master\TenantMaster\TenantMasterModel;

interface ConnectionRepositoryInterface {

    public function connectToTenantDatabase (TenantMasterModel $tenant);

    public function databaseExist ($tenant_id);
}
