<?php

namespace App\Models\Tenant\UserTenant;

use Illuminate\Support\Str;
use App\Models\Tenant\Role\RoleModel;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tenant\Branch\BranchModel;
use App\Models\Master\User\UserMasterModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Tenant\PermissionTenant\PermissionTenantModel;

class UserTenantModel extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql_tenant';

    protected $fillable = [
        'user_id', 'complete_name', 'address', 'gender', 'phone', 'path_img', 'branch_id', 'created_by', 'updated_by',
    ];

    protected $table = 'tn_user_tenant';


    public function user_master() {
        return $this->belongsTo(UserMasterModel::class, 'user_id');
    }

    public function tenant_roles(){
        return $this->belongsToMany(RoleModel::class, 'roles_user_tenant', 'user_tenant_id', 'roles_id');
    }

    public function branch()
    {
        return $this->belongsTo(BranchModel::class, 'branch_id');
    }

    /*
    * About Roles in laravel
    *  https://medium.com/@ezp127/laravel-5-4-native-user-authentication-role-authorization-3dbae4049c8a
    */
    public function authorizeRoles($roles) {

        if (is_array($roles)) {

            return $this->hasAnyRole($roles) || abort(401, 'This action is unauthorized.');
        }

        return $this->hasRole($roles) || abort(401, 'This action is unauthorized.');
    }

    /**
    * Check multiple roles
    * @param array $roles
    */
    public function hasAnyRole($roles){
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
    * Check one role
    * @param string $role
    */
    public function hasRole($role) {
    return null !== $this->roles()->where('name', $role)->first();
    }
}
