<?php

namespace App\Models\Tenant\Menu;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tenant\Menu\MenuTenantModel;

class ChildMenuTenantModel extends Model
{
    protected $connection = 'mysql_tenant';

    protected $table = 'tenant_menu_child';

    protected $fillable = [
        'menu_tenant_id', 'code', 'name', 'url', 'sort', 'description', 'is_active', 'created_by', 'updated_by'
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function parent_menu()
    {
        return $this->belongsTo(MenuTenantModel::class, 'menu_tenant_id');
    }
}
