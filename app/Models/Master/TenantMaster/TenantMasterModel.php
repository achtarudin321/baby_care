<?php

namespace App\Models\Master\TenantMaster;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ConnectionTrait\ConnectionMaster;

class TenantMasterModel extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql_master';

    protected $table = 'ms_tenant';

    protected $fillable = [
        'code', 'npwp', 'name', 'owner', 'phone', 'fax', 'email', 'address',
        'description', 'db_host', 'db_port', 'db_name', 'db_username', 'db_password',
        'is_active', 'created_by', 'updated_by',
    ];

    protected $casts = [
        'is_active'  =>  'boolean',
    ];

    protected static $descriptionUserModel = 'App\Models\Master\DescriptionUser\DescriptionMasterUserModel';

    public function descriptionUser()
    {
        return $this->hasOne(static::$descriptionUserModel, 'tenant_id', 'id');
    }
}
