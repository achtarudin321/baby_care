<?php

namespace App\Models\Master\User;

use Cartalyst\Sentinel\Activations\EloquentActivation;

class ActivationModel extends EloquentActivation
{
    protected $connection = 'mysql_master';

    protected $table = 'ms_activations';

    protected $fillable = [
        'code',
        'completed',
        'completed_at',
    ];

}
