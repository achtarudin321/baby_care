<?php

namespace App\Models\Master\User;

use Cartalyst\Sentinel\Roles\EloquentRole;
use App\Models\Master\TenantMaster\TenantMasterModel;

class RoleModel extends EloquentRole
{
    protected $connection = 'mysql_master';

    protected static $usersModel = 'App\Models\Master\User\UserMasterModel';

    protected $table = 'ms_roles';

    protected $fillable = [
        'name',
        'slug',
        'permissions',
        'tenant_id',
        'is_active'
    ];

    public function delete()
    {
        $isSoftDeleted = array_key_exists('Illuminate\Database\Eloquent\SoftDeletes', class_uses($this));

        if ($this->exists && ! $isSoftDeleted) {
            $this->users()->detach();
        }

        return parent::delete();
    }

    public function users()
    {
        return $this->belongsToMany(static::$usersModel, 'ms_role_users', 'role_id', 'user_id')->withTimestamps();
    }

    public function tenant()
    {
        return $this->belongsTo(TenantMasterModel::class, 'tenant_id');
    }
}
