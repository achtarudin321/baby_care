<?php

namespace App\Models\Master\User;

use App\ConnectionTrait\MasterConnection;
use Cartalyst\Sentinel\Throttling\EloquentThrottle;

class ThrottleModel extends EloquentThrottle
{
    protected $connection = 'mysql_master';

    protected $table = 'ms_throttle';

    protected $fillable = [
        'ip',
        'type',
    ];

}
