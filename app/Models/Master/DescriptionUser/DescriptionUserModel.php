<?php

namespace App\Models\Master\DescriptionUser;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ConnectionTrait\ConnectionMaster;

class DescriptionUserModel extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql_master';

    protected $table = 'ms_description_users';

    protected $fillable = [
        'user_id',
        'tenant_id',
        'description',
        'is_active'
    ];

    protected $casts = [
        'is_active'  =>  'boolean',
    ];

    protected static $tenantMaster = 'App\Models\Master\TenantMaster\TenantMasterModel';

    public function tenant_master()
    {
        return $this->belongsTo(static::$tenantMaster, 'tenant_id')->withDefault();
    }
}
